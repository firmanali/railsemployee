Rails.application.routes.draw do
  resources :laptops
  resources :employees
  resources :branches
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
