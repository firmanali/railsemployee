class CreateLaptops < ActiveRecord::Migration[5.2]
  def change
    create_table :laptops do |t|
      t.string :brand
      t.string :tipe
      t.references :employee, foreign_key: true
      t.boolean :is_active

      t.timestamps
    end
  end
end
