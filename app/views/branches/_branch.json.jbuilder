json.extract! branch, :id, :address, :phone, :created_at, :updated_at
json.url branch_url(branch, format: :json)
